<?php
/**
 * Plugin Name: UoR Overall Google Analytics
 * Plugin URI: http://research.reading.ac.uk/act
 * Version: 1.0
 * Author: Eric MATHIEU (Academic Computing Team)
 * Author URI: http://research.reading.ac.uk/act
 * Description: Adds the UoR RCE google analytics snippet on top of all pages where this plugin is activated
 * License: GPL2
 */
 
//Google multisite tracking ID: 
$trackingID = "UA-127668464-1"; //UA-127668464-1 = Research WP property, managed by WPResearch One (RCE)

function uor_analytics($trackingID) {
    if (!is_user_logged_in()) {
		?>		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $GLOBALS['trackingID']; ?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo $GLOBALS['trackingID']; ?>');
		</script>
    <?php }
}
add_action( 'wp_head', 'uor_analytics', 1 );


?>
