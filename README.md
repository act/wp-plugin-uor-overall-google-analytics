Adds a google analytics property to all sites of the multi-sites installation where this plugin is activated. 
Note that the plugin is fairly simple and the google analytics property is hard coded!